#include "testApp.h"  

//--------------------------------------------------------------
// crazy cool functions
//--------------------------------------------------------------
void zuckerBrotUndPeitsche(ofImage* screen, aPlayer * r, aPlayer * l){
    ofPixels *pixels = &screen->getPixelsRef(); // screen is a .grabScreen(0, 0, w, h); from the flowShader-fbo
    
    int w = 640;
    int h = 480;
    
    int lc = 0; // count pixelbrightness for left side
    int rc = 0; // count pixelbrightness for right side
    
    int c = 0;
    for (int y = 0; y < h; y++){
        for (int x = 0; x < w; x++){
            if (x < w/2){
                //right
                rc += pixels->getColor(x, y).getBrightness();
            } else {
                //left
                lc += pixels->getColor(x, y).getBrightness();
            }
            c = c + 3;
        }
    }
    rc /= w*h;
    lc /= w*h;
    r->brightness = rc * 0.1 + r->brightness * 0.9;
    l->brightness = lc * 0.1 + r->brightness * 0.9;
    r->currentscore += 4*(float)(rc)/255.0;
    l->currentscore += 4*(float)(lc)/255.0;
    
}

void calculateRound(aPlayer * r, aPlayer * l) {
//    float * right = &r->currentscore;
//    float * left = &l->currentscore;
    if(r->currentscore == l->currentscore){
        // nothing
    } else if (r->currentscore > l->currentscore) {
        r->wins++;
    } else {
        l->wins++;
    }
    r->scoreHistory.push_back(r->currentscore);
    l->scoreHistory.push_back(l->currentscore);
    r->currentscore = 0.0;
    l->currentscore = 0.0;
}

//--------------------------------------------------------------
void testApp::setup(){
	int w,h;
	w=640;
	h=480;
	cam.initGrabber(w, h, true);  
	cam.setUseTexture(true);  

	ofSetBackgroundAuto(false);
	ofEnableAlphaBlending();

	mioFlow.setup(w,h);
    screenshotMade = false;
    getWinner = true;
    font.loadFont("fonts/AbsaraSans.otf", 144);
}

//--------------------------------------------------------------  
void testApp::update(){  
	cam.update();
	if (cam.isFrameNew()) {
		float valX=(float)ofGetMouseX()/(float)ofGetWidth();
		float valY=(float)ofGetMouseY()/(float)ofGetHeight();

		mioFlow.update(cam.getTextureReference(),valX,valY*10,0.5);
        screen = &mioFlow.screenshot;
        zuckerBrotUndPeitsche(screen, &theGame.r, &theGame.l);
        screenshotMade = true;
	}
    if(theGame.r.currentscore >= 100.0 || theGame.l.currentscore >= 100.0){
        if(getWinner) {
            calculateRound(&theGame.r, &theGame.l);
            getWinner = false;
        }
    } else {
        getWinner = true;
    }
    ofSetWindowTitle(ofToString(ofGetFrameRate()));
}  

//--------------------------------------------------------------  
void testApp::draw(){
//	mioFlow.drawReposition(0,0);
//	mioFlow.drawFlowGridRaw(640,0);
//	mioFlow.drawFlowGrid(0,480);
    if(screenshotMade) {
        ofPushMatrix();
        ofRotate(180);
        ofTranslate(-ofGetWidth(),-ofGetHeight());
        screen->draw(0,0,ofGetWidth(),ofGetHeight());
        ofPopMatrix();
    }
    
    
    ofSetColor(0,255,255);
    font.drawString(ofToString(theGame.l.wins), ofGetWidth()/4, ofGetHeight()/2);
    ofSetColor(255,0,255);
    font.drawString(ofToString(theGame.r.wins), 3*ofGetWidth()/4, ofGetHeight()/2);
    
    ofSetColor(125,255,125);
    ofSetLineWidth(10);
    ofLine(ofGetWidth()/2,0,ofGetWidth()/2,ofGetHeight());
    
    // SCOOOOORE
    ofSetColor(
               //               min(theGame.r.brightness*15,255),
               255,
               min(theGame.l.brightness*20,255),
               min(theGame.l.brightness*30,255)
               );
    ofRect(ofGetWidth()/2 - 500,40,abs(theGame.l.currentscore) * 5, 10);
    ofSetColor(
               min(theGame.r.brightness*30,255),
               255,
               //               0,
               min(theGame.r.brightness*20,255)               );
    ofRect(ofGetWidth()/2 + 500,40,-1 * abs(theGame.r.currentscore) * 5, 10);
    
    ofPushStyle();
    ofNoFill();
    ofSetLineWidth(2);
    ofSetColor(0,255,255);
    ofRect(ofGetWidth()/2 - 500,40, 500, 10);
    ofSetColor(255,0,255);
    ofRect(ofGetWidth()/2 + 500,40, -500, 10);
    ofSetColor(255);
    
    ofSetColor(125,255,125);
    ofSetLineWidth(10);
    ofLine(ofGetWidth()/2,0,ofGetWidth()/2,ofGetHeight());
    ofPopStyle();
    ofDrawBitmapString("wins: " + ofToString(theGame.r.wins) + "\nscore: " + ofToString(theGame.r.currentscore), ofGetWidth()-150, 20);
    ofDrawBitmapString("wins: " + ofToString(theGame.l.wins) + "\nscore: " + ofToString(theGame.l.currentscore), 150, 20);
    
    ofDrawBitmapString("sensitivity (change with up/down) : " + ofToString(mioFlow.sensitivity), 150, ofGetHeight() - 20);
}

void testApp::keyReleased( int key ) {
    if (key == OF_KEY_UP) {
        mioFlow.sensitivity += 1.0;
    } else if (key == OF_KEY_DOWN) {
        mioFlow.sensitivity -= 1.0;
    }
}
So here are some experiments for the class.
exampleOpticalFlow is a simple example of an optical flow using ofxMIOFlowGLSL.
opticalFlowGame is an example of how you might make a game of it by splitting the screen into left and right and counting the movement on each side.
The side that first reaches a specified amount of total movement (fill the bar) get's a point.

If you just want to try out, just download the opticalFlowGame.zip

dependencies:
openFrameworks 0.8.x 
ofxMIOFlowGLSL

#pragma once

#include "ofMain.h"
#include "ofxMioFlowGLSL.h"

struct aPlayer {
    vector<float> scoreHistory;
    float currentscore = 0.0;
    int wins = 0;
    int brightness = 0;
};

struct aGame {
    aPlayer r;
    aPlayer l;
    int getTotalscore(){
        int t = r.wins - l.wins;
        return t;
    }
};

class testApp : public ofBaseApp{  
    public:  
        void setup();  
        void update();  
        void draw();
        void keyReleased( int key );

        ofVideoGrabber cam;
		ofxMioFlowGLSL mioFlow;
        ofImage * screen;
        bool screenshotMade;
        aGame theGame;
        bool getWinner;
    
        ofTrueTypeFont font;
};